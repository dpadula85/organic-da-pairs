#!/usr/bin/env python

import numpy as np
from sklearn.kernel_ridge import KernelRidge
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import LeaveOneOut


# Define function to be minimised by DE
def mini_fn(hyperparams, kernel, inputs, weighted=False):

    X, y = inputs
    alpha, gamma1, gamma2, gamma3 = hyperparams

    # Build kernel function
    kernel = kernel(gamma1=gamma1,
                    gamma2=gamma2,
                    gamma3=gamma3)

    # Build KRR estimator
    krr = KernelRidge(alpha=alpha, kernel=kernel)

    # LOO loop
    loo = LeaveOneOut()
    y_true = []
    y_pred = []
    for i, idxs in enumerate(loo.split(X), start=1):

        tr_idx, ts_idx = idxs
        Xtr = X[tr_idx]
        ytr = y[tr_idx]
        Xts = X[ts_idx]
        yts = y[ts_idx]

        krr.fit(Xtr, ytr)
        y_hat = krr.predict(Xts)
        y_true.append(yts)
        y_pred.append(y_hat)

    y_true = np.array(y_true).flatten()
    y_pred = np.array(y_pred).flatten()

    if weighted:
        weights = y_true**2 / np.linalg.norm(y_true**2)
    else:
        weights = np.ones_like(y_true)

    mse = mean_squared_error(y_true, y_pred, sample_weight=weights)
    rmse = np.sqrt(mse)

    return rmse


if __name__ == '__main__':
    pass
