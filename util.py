#!/usr/bin/env python

import sys


def unpack(n, seq):

    it = iter(seq)
    for _ in range(n - 1):
        yield next(it, None)

    yield tuple(it)


def banner(text=None, ch='=', length=78):
    """Return a banner line centering the given text.

        "text" is the text to show in the banner. None can be given to have
            no text.
        "ch" (optional, default '=') is the banner line character (can
            also be a short string to repeat).
        "length" (optional, default 78) is the length of banner to make.

    Examples:
        >>> banner("Peggy Sue")
        '================================= Peggy Sue =================================='
        >>> banner("Peggy Sue", ch='-', length=50)
        '------------------- Peggy Sue --------------------'
        >>> banner("Pretty pretty pretty pretty Peggy Sue", length=40)
        'Pretty pretty pretty pretty Peggy Sue'
    """
    if text is None:
        return ch * length

    elif len(text) + 2 + len(ch)*2 > length:
        # Not enough space for even one line char (plus space) around text.
        return text

    else:
        remain = length - (len(text) + 2)
        prefix_len = remain / 2
        suffix_len = remain - prefix_len

        if len(ch) == 1:
            prefix = ch * prefix_len
            suffix = ch * suffix_len

        else:
            prefix = ch * (prefix_len/len(ch)) + ch[:prefix_len%len(ch)]
            suffix = ch * (suffix_len/len(ch)) + ch[:suffix_len%len(ch)]

        return prefix + ' ' + text + ' ' + suffix


def transl_dict(v):

      if type(v) is str:
        w = v

      elif type(v) is int or type(v) is float:
        w = str(v)

      elif type(v) is list:
        w = ', '.join(map(str, v))

      elif type(v) is dict:
        lenv = len(v)
        w = [None]*lenv
        for kk, vv in v.items():
          w[kk - 1] = transl_dict(vv)

      elif hasattr(v, '__call__'):
        w = v.__name__

      else:
        w = v

      return w


def print_dict(opts_dict, title=None, ch="-", outstream=None):

    if outstream:
      sys.stdout = outstream

    if not title:
      title = "Options"

    print("#" + banner(text=title, ch=ch, length=79))
    fmt = "# %-18s %-20s"
    for k, v in sorted(opts_dict.items()):
        if type(v) in [str, int, float, bool, list] or hasattr(v, '__call__'):
            w = transl_dict(v)
            print(fmt % (k, w))

        elif type(v) is dict:
            print_dict(v, title=k, ch="-")

    print("#" + banner(ch=ch, length=79))

    return


if __name__ == '__main__':
    pass
