#!/usr/bin/env python

import sys
import time
import warnings
import numpy as np
import pandas as pd
from datetime import timedelta
from sklearn.externals import joblib
from scipy.optimize._differentialevolution import DifferentialEvolutionSolver

import util as u
from opts import options
from mini import mini_fn
from kernels import build_hybrid_kernel
from preprocess import preprocess_smiles, preprocess_fn

# data columns in the df
xcols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
        "Lambda-", "DonorFP", "AcceptorFP" ]

ycols = [ "PCE" ]


def warn(*args, **kwargs):
    '''
    Suppress Scikit-learn warnings.
    '''
    pass


def main():

    warnings.warn = warn
    Opts = options()

    if Opts['OutFile'] is not None:
        sys.stdout = open(Opts['OutFile'], "wb")

    u.print_dict(Opts, title="Options")

    try:
        df = pd.read_excel(Opts['DBFile'], index_col=0)
    except:
        df = pd.read_csv(Opts['DBFile'], index_col=0)

    # Preprocess SMILES
    df = preprocess_smiles(df)

    # Get data
    X = df[ xcols ].values
    y = df[ ycols ].values

    # Preprocess electronic data to 0 mean and 1 sigma
    # Stack fingerprints
    X = preprocess_fn(X)

    # Define input tuple
    inputs = (X, y)

    # Define if weighted RMSE should be minimised
    weighted = Opts['Weighted']

    # Define bounds for hyperparams, maybe to move into input options
    alpha_lim = (0, 2)
    gamma_lim = (0, 10)

    # Define kernel, hyperparams, and bounds for each ### TO DO
    bounds = [ alpha_lim ] + [ gamma_lim ] * 3
    hyperparams_names = [ "alpha" ] + [ "gamma%d" % i for i in range(1, 4) ]

    #
    # Optimise hyperparams
    #

    # Headers
    header = [ "Step" ] + hyperparams_names + [ "RMSE" ]
    fmt = "# %-8s" + "%12s" * (len(header) - 1)

    if Opts['Verb'] > 0:
        print "#"
        print "#" + u.banner(text="Differential Evolution", ch="-", length=79)
        print fmt % tuple(header)
        print "#" + u.banner(ch="-", length=79)

    # Format
    fmt = "# %-8d" + "%12.6f" * len(hyperparams_names) + "%12.6f"

    # Save all evaluations to plot RMSE map in hyperparams space
    try:
        filename = ''.join(Opts['OutFile'].split(".")[:-1])
        filename += "_costmap.dat"
    except:
        filename = "costmap.dat"

    names = hyperparams_names + [ "Metric" ]
    hdr = "\n%8s" + " %10s" * (len(names) - 1) + "\n"
    hdr = hdr % tuple(names)

    # Initialise DE solver
    mini_args = (build_hybrid_kernel, inputs, weighted)
    solver = DifferentialEvolutionSolver(mini_fn,
                                         bounds,
                                         args=mini_args,
                                         popsize=50,
                                         # tol=0.10,
                                         polish=False,
                                         workers=-1,
                                         updating='deferred')

    # DE initial state
    params = solver.population
    params = solver._scale_parameters(params)
    enes = solver._calculate_population_energies(params)
    solver._promote_lowest_energy()
    evaluations = np.c_[ params, enes ]
    np.savetxt(filename, evaluations, fmt="%10.6f", header=hdr)

    # Begin DE iterations
    start = time.time()
    for i in np.arange(0, 1e6):

        best_x, best_cost = next(solver)
        info = [ i ] + best_x.tolist() + [ best_cost ]

        if Opts['Verb'] > 0:
            print fmt % tuple(info)
        sys.stdout.flush()

        # Get evaluations to plot RMSE in hyperparams space
        params = solver.population
        params = solver._scale_parameters(params)
        enes = solver.population_energies
        evals = np.c_[ params, enes ]
        evaluations = np.r_[ evaluations, evals ]
        np.savetxt(filename, evaluations, fmt="%10.6f", header=hdr)

        # Convergence criterion implemented in DESolver class
        check = np.std(enes) <= solver.atol + solver.tol * np.abs(np.mean(enes))
        if check:
            break

    if Opts['Verb'] > 0:
        print "#" + u.banner(ch="-", length=79)

    # Get evaluations to plot RMSE in hyperparams space
    params = solver.population
    params = solver._scale_parameters(params)
    enes = solver.population_energies
    evals = np.c_[ params, enes ]
    evaluations = np.r_[ evaluations, evals ]
    np.savetxt(filename, evaluations, fmt="%10.6f", header=hdr)

    # Print elapsed time
    elapsed = time.time() - start
    timestr = str(timedelta(seconds=elapsed))

    # Get best hyperparams
    hyperparams = solver.x

    print "#"
    print "#" + u.banner(text="Differential Evolution Report", ch="-",
                         length=79)

    for i, hypparam in enumerate(hyperparams):
        print "# %-14s %10.6f" % (hyperparams_names[i], hypparam)

    print "# Metric         %10.6f" % best_cost
    print "# Function Evals %10d" % solver._nfev
    print "# Time Elapsed   %10s" % timestr.split(".")[0]
    print "#" + u.banner(ch="-", length=79)

    sys.stdout.close()
    sys.stdout = sys.__stdout__

    return


if __name__ == '__main__':
    main()
