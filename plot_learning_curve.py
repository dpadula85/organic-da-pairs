#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse as arg
import matplotlib.pyplot as plt
from matplotlib import ticker, gridspec, cm
from sklearn.kernel_ridge import KernelRidge
from sklearn.metrics import mean_squared_error, make_scorer
from sklearn.model_selection import LeaveOneOut, learning_curve

from kernels import build_hybrid_kernel
from preprocess import preprocess_smiles, preprocess_fn

# data columns in the df
xcols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
        "Lambda-", "DonorFP", "AcceptorFP" ]

ycols = [ "PCE" ]


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(
                formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--db', required=True, type=str, dest='DBFile',
                     help='''Database File.''')

    #
    # Calculations Options
    #
    calc = parser.add_argument_group("Calculation Options")

    calc.add_argument('--alpha', required=True, type=float, dest='Alpha',
                     help='''Regularisation Hyperparameter.''')

    calc.add_argument('--gamma1', required=True, type=float, dest='Gamma1',
                     help='''Electronic Distance Hyperparameter.''')

    calc.add_argument('--gamma2', required=True, type=float, dest='Gamma2',
                     help='''Donors Tanimoto Distance Hyperparameter.''')

    calc.add_argument('--gamma3', required=True, type=float, dest='Gamma3',
                     help='''Acceptors Tanimoto Distance Hyperparameter.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")

    out.add_argument('-o', '--output', default=None, type=str, dest='OutFile',
                     help='''Output File.''')

    out.add_argument('-v', '--verbosity',
                     default=0, action="count", dest="Verb",
                     help='''Verbosity level''')

    args = parser.parse_args()
    Opts = vars(args)

    return Opts


def plot_learning_curve(estimator, X, y, cv=None, n_jobs=None, scoring=None,
                        train_sizes=np.linspace(0.2, 1.0, 5)):
    """
    Generate a simple plot of the test and training learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    cv : int, cross-validation generator or an iterable, optional
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:
          - None, to use the default 3-fold cross-validation,
          - integer, to specify the number of folds.
          - :term:`CV splitter`,
          - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : int or None, optional (default=None)
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    train_sizes : array-like, shape (n_ticks,), dtype float or int
        Relative or absolute numbers of training examples that will be used to
        generate the learning curve. If the dtype is float, it is regarded as a
        fraction of the maximum size of the training set (that is determined
        by the selected validation method), i.e. it has to be within (0, 1].
        Otherwise it is interpreted as absolute sizes of the training sets.
        Note that for classification the number of samples usually have to
        be big enough to contain at least one sample from each class.
        (default: np.linspace(0.1, 1.0, 5))
    """

    fig = plt.figure(figsize=(19, 11.2))
    gs = gridspec.GridSpec(1, 1)
    ax = plt.subplot(gs[0])

    _, train_scores, test_scores = learning_curve(estimator, X, y, cv=cv,
                                                  n_jobs=n_jobs,
                                                  train_sizes=train_sizes,
                                                  scoring=scoring)

    train_sizes *= 100
    train_scores_mean = np.mean(np.sqrt(train_scores), axis=1)
    test_scores_mean = np.mean(np.sqrt(test_scores), axis=1)

    ax.plot(train_sizes, train_scores_mean, 'o-', color="r",
            label="Training")
    ax.plot(train_sizes, test_scores_mean, 'o-', color="g",
            label="LOO")

    ax.set_xlabel("Training examples %", size=24)
    ax.set_ylabel("RMSE", size=24)
    ax.tick_params(axis='both', which='major', direction='in', labelsize=22, pad=10, length=5)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=22, pad=10, length=2)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.grid()

    ax.legend(fontsize=22, loc="best").draw_frame(False)

    return plt


if __name__ == '__main__':

    Opts = options()

    df = pd.read_csv(Opts['DBFile'], index_col=0)

    # Preprocess SMILES
    df = preprocess_smiles(df)

    # Get data
    X = df[ xcols ].values
    y = df[ ycols ].values

    # Preprocess electronic data to 0 mean and 1 sigma
    # Stack fingerprints
    X = preprocess_fn(X)

    # Build kernel function
    kernel = build_hybrid_kernel(gamma1=Opts["Gamma1"],
                                 gamma2=Opts["Gamma2"],
                                 gamma3=Opts["Gamma3"])
    
    # Build KRR estimator
    krr = KernelRidge(alpha=Opts["Alpha"], kernel=kernel)
    
    # LOO loop
    loo = LeaveOneOut()
    mse = make_scorer(mean_squared_error)
    plot_learning_curve(krr, X, y, cv=loo, scoring=mse)

    plt.savefig("data/figs/model3_lc.pdf", dpi=600, bbox_inches='tight')
    # plt.show()
