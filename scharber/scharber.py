#!/usr/bin/env python

import numpy as np
from scipy.constants import *
import matplotlib.pyplot as plt
from matplotlib import ticker, gridspec, cm
from mpl_toolkits.axes_grid1 import ImageGrid

# Conversion factor and solar spectrum.
eV2nm = 1239.8
spectrum = np.loadtxt("ASTMG173.csv")
x_spec = spectrum[:,0]
y_spec = spectrum[:,1]


def scharber(homod, lumod, lumoa, loss=0.3, fac=0.65, FF=0.65, Pin=1000.0):
    '''
    Function to compute the efficiency of a donor-acceptor pair according to
    Scharber's model.

    Parameters
    ----------
    homod: float.
        donor's HOMO energy.
    lumod: float.
        donor's LUMO energy.
    lumoa: float.
        acceptor's LUMO energy.
    loss: float.
        empirical loss factor for charge separation.
    fac: float.
        empirical scaling factor for light absorption.
    FF: float.
        empirical fill factor.
    Pin: float.
        incident light power.

    Returns
    -------
    eta: float.
        % efficiency of the solar cell with the given electronic parameters
        estimated according to Scharber.
    '''

    DA_gap = lumoa - homod
    D_gap = lumod - homod

    Voc_Sch = scharber_Voc(DA_gap, loss)
    Jsc_Sch = scharber_Jsc(D_gap, fac)
    eta = Voc_Sch * Jsc_Sch * FF / Pin

    return eta * 100.0


def scharber_Voc(gap, loss=0.3):
    '''
    Function to compute the open circuit voltage of a donor-acceptor pair
    according to Scharber's model.

    Parameters
    ----------
    gap: float.
        energy gap between the acceptor's LUMO and donor's HOMO.
    loss: float.
        empirical loss factor for the model.

    Returns
    -------
    Voc: float.
        open circuit voltage.
    '''

    Voc = gap - loss

    return Voc


def scharber_Jsc(gap, fac=0.65):
    '''
    Function to compute the short circuit current of a donor-acceptor pair
    according to Scharber's model.

    Parameters
    ----------
    gap: float.
        energy gap between the donor's LUMO and donor's HOMO.

    Returns
    -------
    Jsc: float.
        short circuit current.
    '''

    wavelength = eV2nm / gap

    idxs = (x_spec <= wavelength)

    # Convert to SI units (nm -> m).
    newx = np.copy(x_spec)[idxs] * 1e-9
    newy = np.copy(y_spec)[idxs] / 1e-9

    # This uses some constants from SciPy, e, h, c.
    Jph = e * np.trapz(newy * newx / (h * c), x=newx)

    # Convert from A / m**2 to mA / cm**2.
    # Jph /= 10.0

    # Scale empirically.
    Jsc = Jph * fac

    return Jsc


if __name__ == '__main__':

    import csv
    import pandas as pd

    df = pd.read_csv("../data/db_available.csv", index_col=0)
    cols = [ "DonorNo", "AcceptorNo", "HOMO_D", "LUMO_D", "LUMO_A", "V_OC", "J_SC", "PCE" ]
    df = df[cols]
    df = df[df["LUMO_D"] >= df["LUMO_A"]]
    df["DA_GAP"] = df["LUMO_A"] - df["HOMO_D"]
    df["D_GAP"] = df["LUMO_D"] - df["HOMO_D"]
    df["Scharber_V_OC"] = df["DA_GAP"].map(scharber_Voc)
    df["Scharber_J_SC"] = df["D_GAP"].map(scharber_Jsc)
    df["Scharber_PCE"] = 100 * df["Scharber_V_OC"] * df["Scharber_J_SC"] * 0.65 / 1000
    df["Scharber_J_SC"] /= 10
    df = df.drop([ "HOMO_D", "LUMO_D", "LUMO_A", "DA_GAP", "D_GAP" ], axis=1)
    df.to_csv("db_available_scharber.csv", quoting=csv.QUOTE_NONNUMERIC)

    df = pd.read_csv("../data/db_unexplored.csv", index_col=0)
    cols = [ "DonorNo", "AcceptorNo", "HOMO_D", "LUMO_D", "LUMO_A" ]
    df = df[cols]
    df = df[df["LUMO_D"] >= df["LUMO_A"]]
    df["DA_GAP"] = df["LUMO_A"] - df["HOMO_D"]
    df["D_GAP"] = df["LUMO_D"] - df["HOMO_D"]
    df["Scharber_V_OC"] = df["DA_GAP"].map(scharber_Voc)
    df["Scharber_J_SC"] = df["D_GAP"].map(scharber_Jsc)
    df["Scharber_PCE"] = 100 * df["Scharber_V_OC"] * df["Scharber_J_SC"] * 0.65 / 1000
    df["Scharber_J_SC"] /= 10

    df = df.drop([ "HOMO_D", "LUMO_D", "LUMO_A", "DA_GAP", "D_GAP" ], axis=1)
    df.to_csv("db_unexplored_scharber.csv", quoting=csv.QUOTE_NONNUMERIC)

    # # Grid test
    # HOMO_D = 0.0
    # LUMO_A = np.arange(0.3, 4.0, 0.05)
    # LUMO_D = np.arange(0.3, 4.0, 0.05)

    # results = []
    # for la in LUMO_A:
    #     for ld in LUMO_D:

    #         if ld > la:
    #             eta = scharber(HOMO_D, ld, la)
    #             results.append([ld, la, eta])

    # results = np.array(results)
    # x, y, z = results.T

    # fig = plt.figure(figsize=(11,11))

    # grid = ImageGrid(fig, 111,
    #                  nrows_ncols=(1,1),
    #                  axes_pad=0.15,
    #                  share_all=True,
    #                  cbar_location="right",
    #                  cbar_mode="single",
    #                  cbar_size="2%",
    #                  cbar_pad=0.15)

    # ax = grid[0]
    # sc = ax.scatter(x, y, c=z, s=80, cmap="gnuplot")

    # ax.tick_params(axis='both', which='major', direction='in', labelsize=22, pad=10, length=5)

    # ax.set_xlabel(r"$E^{gap}_D$ / eV", size=24, labelpad=10)
    # ax.set_ylabel(r"$E^{LUMO}_A$ / eV", size=24, labelpad=10)

    # xtickmaj = ticker.MaxNLocator(5)
    # xtickmin = ticker.AutoMinorLocator(5)
    # ytickmaj = ticker.MaxNLocator(5)
    # ytickmin = ticker.AutoMinorLocator(5)
    # ax.xaxis.set_major_locator(xtickmaj)
    # ax.xaxis.set_minor_locator(xtickmin)
    # ax.yaxis.set_major_locator(ytickmaj)
    # ax.yaxis.set_minor_locator(ytickmin)
    # ax.xaxis.set_ticks_position('both')
    # ax.yaxis.set_ticks_position('both')
    # ax.tick_params(axis='both', which='minor', direction='in', labelsize=22, pad=10, length=2)
    # ax.set_aspect('equal')

    # # # Colorbar
    # sm = plt.cm.ScalarMappable(cmap="gnuplot")
    # sm._A = []
    # ax.cax.colorbar(sc)
    # ax.cax.set_ylabel(r"$\eta$ / %", size=24, labelpad=10)
    # ax.cax.tick_params(labelsize=22)
    # ax.cax.toggle_label(True)

    # plt.show()
