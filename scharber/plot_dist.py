#!/usr/bin/env python

import warnings
warnings.filterwarnings("ignore")

import sys
import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt
from matplotlib import ticker

if __name__ == '__main__':

    df = pd.read_csv("db_available_scharber.csv", index_col=0)
    df = df[df["Scharber_J_SC"] > 0 ]
    df = df[df["Scharber_PCE"] > 0 ]
    x = df["Scharber_PCE"]
    nbins = int(np.log2(x.shape[0]) + 1)
    avg = np.mean(x)
    sigma = np.std(x)

    # Scatter plot
    fig, ax = plt.subplots(figsize=(19.2,11))
    n, bins, patches = ax.hist(x, bins=nbins, histtype='bar', rwidth=0.75,
                               hatch='//', fill=False, color='b', edgecolor='b')


    # Fancy stuff
    ax.set_xlabel(u"$\eta$ / %", size=24)
    ax.set_ylabel(u"Count", size=24)
    ax.tick_params(axis='both', which='major', direction='in', labelsize=24, pad=10, length=7)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=24, pad=10, length=3)
    xtickmaj = ticker.MultipleLocator(2)
    xtickmin = ticker.MultipleLocator(0.5)
    ytickmaj = ticker.MultipleLocator(10)
    ytickmin = ticker.MultipleLocator(2)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)
    # ax.yaxis.set_major_locator(ytickmaj)
    # ax.yaxis.set_minor_locator(ytickmin)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.minorticks_on()
    ax.annotate(u'$\eta_{avg}$ = %.2f' % avg, xy=(0.15,0.8), xycoords='axes fraction', size=24)
    ax.annotate(u'$\sigma_{\eta}$ = %.2f' % sigma, xy=(0.15,0.7), xycoords='axes fraction', size=24)

    plt.savefig("../data/figs/scharber_available_distribution.pdf", dpi=600, bbox_inches='tight', figsize=(19.2,11))
    # plt.show()


    df = pd.read_csv("db_unexplored_scharber.csv", index_col=0)
    df = df[df["Scharber_J_SC"] > 0 ]
    df = df[df["Scharber_PCE"] > 0 ]
    x = df["Scharber_PCE"]
    nbins = int(np.log2(x.shape[0]) + 1)
    avg = np.mean(x)
    sigma = np.std(x)

    # Scatter plot
    fig, ax = plt.subplots(figsize=(19.2,11))
    n, bins, patches = ax.hist(x, bins=nbins, histtype='bar', rwidth=0.75,
                               hatch='//', fill=False, color='b', edgecolor='b')


    # # Fit a gaussian, scaled to the real distribution of the data and add it to the legend
    # scale_factor = (bins[1] - bins[0]) * x.shape[0]
    # fitx = np.linspace(x.min(), x.max(), 1000)
    # gau_fit = norm.pdf(fitx, avg, sigma) * scale_factor
    # gau_line = ax.plot(fitx, gau_fit, '-', linewidth=2, color='b')

    # Fancy stuff
    ax.set_xlabel(u"$\eta$ / %", size=24)
    ax.set_ylabel(u"Count", size=24)
    ax.tick_params(axis='both', which='major', direction='in', labelsize=24, pad=10, length=7)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=24, pad=10, length=3)
    xtickmaj = ticker.MultipleLocator(2)
    xtickmin = ticker.MultipleLocator(0.5)
    ytickmaj = ticker.MultipleLocator(1000)
    ytickmin = ticker.MultipleLocator(200)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)
    # ax.yaxis.set_major_locator(ytickmaj)
    # ax.yaxis.set_minor_locator(ytickmin)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.minorticks_on()
    ax.annotate(u'$\eta_{avg}$ = %.2f' % avg, xy=(0.55,0.8), xycoords='axes fraction', size=24)
    ax.annotate(u'$\sigma_{\eta}$ = %.2f' % sigma, xy=(0.55,0.7), xycoords='axes fraction', size=24)

    plt.savefig("../data/figs/scharber_unexplored_distribution.pdf", dpi=600, bbox_inches='tight')
    # plt.show()
