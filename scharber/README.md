Description
===========
In this folder there are:

- the database for experimentally available donor-acceptor pairs
(`db_available_scharber.csv`)

- the database for experimentally unexplored donor-acceptor pairs
(`db_unexplored_scharber.csv`)

- scripts to produce plots and compute Scharber's model results

Notice that figures will be saved in `.pdf` format in the
`../data/figs/` folder.
