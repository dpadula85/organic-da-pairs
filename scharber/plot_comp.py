#!/usr/bin/env python

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import ticker, gridspec, cm


if __name__ == '__main__':

    df = pd.read_csv("db_available_scharber.csv")
    df = df[df["Scharber_J_SC"] > 0.0]
    df = df[df["Scharber_PCE"] > 0.0]

    exp_names = [ u'V_OC', u'J_SC', u'PCE' ]
    calc_names = [ u'Scharber_V_OC', u'Scharber_J_SC', u'Scharber_PCE' ]
    df_exp_comp = df[exp_names]
    df_calc_comp = df[calc_names]

    n = len(df_exp_comp.columns)
    exp_labels = [u"V$_{OC}$",
                  u"J$_{SC}$",
                  u"$\eta$"]

    calc_labels = [u"V$^{Sch}_{OC}$",
                   u"J$^{Sch}_{SC}$",
                   u"$\eta^{Sch}$"]

    units = [u"V",
             u"mA $\cdot$ cm$^{-2}$",
             u"%"]

    fig = plt.figure(figsize=(9,12))
    gs = gridspec.GridSpec(3, 1)
    gs.update(wspace=0.15, hspace=0.30)
    for i in range(n):
        exp_prop = df_exp_comp.iloc[:,i]
        calc_prop = df_calc_comp.iloc[:,i]

        comp = pd.concat([exp_prop, calc_prop], axis=1)
        r = comp.corr(method="pearson").values[0,1]
        rho = comp.corr(method="spearman").values[0,1]
        tau = comp.corr(method="kendall").values[0,1]

        mi = min([exp_prop.min(), calc_prop.min()])
        mi -= 0.1 * np.abs(mi)
        ma = max([exp_prop.max(), calc_prop.max()])
        ma += 0.1 * np.abs(ma)
        comp = pd.concat([exp_prop, calc_prop], axis=1)

        ax = plt.subplot(gs[i])
        ax.scatter(exp_prop, calc_prop, color="b")
        ax.tick_params(axis='both', which='major', direction='in', labelsize=12, pad=10, length=5)

        ax.set_xlabel("%s / %s" % (exp_labels[i], units[i]), size=14, labelpad=10)
        ax.set_ylabel("%s / %s" % (calc_labels[i], units[i]), size=14, labelpad=10)

        xtickmaj = ticker.MaxNLocator(5)
        xtickmin = ticker.AutoMinorLocator(5)
        ytickmaj = ticker.MaxNLocator(5)
        ytickmin = ticker.AutoMinorLocator(5)
        ax.xaxis.set_major_locator(xtickmaj)
        ax.xaxis.set_minor_locator(xtickmin)
        ax.yaxis.set_major_locator(ytickmaj)
        ax.yaxis.set_minor_locator(ytickmin)
        ax.xaxis.set_ticks_position('both')
        ax.yaxis.set_ticks_position('both')
        ax.tick_params(axis='both', which='minor', direction='in', labelsize=12, pad=10, length=2)
        ax.set_xlim(mi, ma)
        ax.set_ylim(mi, ma)
        ax.set_aspect('equal')

        # xmin, xmax = ax.get_xlim()
        # ymin, ymax = ax.get_ylim()
        ax.plot(np.arange(mi,ma,0.1), np.arange(mi,ma,0.1), color="k", ls="--")
        ax.annotate(u'$r$ = %.2f' % r, xy=(0.65,0.4), xycoords='axes fraction', size=12)
        ax.annotate(u'$\\rho$ = %.2f' % rho, xy=(0.65,0.25), xycoords='axes fraction', size=12)
        ax.annotate(u'$\\tau$ = %.2f' % tau, xy=(0.65,0.10), xycoords='axes fraction', size=12)

    plt.savefig("../data/figs/scharber.pdf", dpi=600, bbox_inches='tight')
    # plt.tight_layout()
    # plt.show()
