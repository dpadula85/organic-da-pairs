Description
===========
This repository contains electronic supporting information for the
paper submitted by D. Padula and A. Troisi.

In this folder there are:

- scripts to train models (`train.py`)

- scripts to compute cross-validation predictions (`eval.py`)

- scripts to compute test sets (`infer.py`)

Notice that minimisation metrics have to be changed manually
in the `mini.py` module, according to the metrics reported in
the paper.

Notice that the optimisation includes an element of randomness.
We did not set any seed value, so the values obtained in the
attempt to reproduce our calculations might be slightly different.

Additional `README.md` files are located in each folder to shortly describe
the content. No instructions are provided, except from comments in the code.

Contact D. Padula (<dpadula85@yahoo.it>) for queries or help on how to use the
provided code.

Requirements
============
The only requirement is:

 - SciPy 1.2

With earlier versions, the code will work (with small changes), except the
training cannot run in parallel.
The code has been tested with the following versions of required libraries:

 - Pandas 0.23.4

 - NumPy 1.15.4

 - RdKit 2018.09.2

 - Scikit-learn 0.20.1
