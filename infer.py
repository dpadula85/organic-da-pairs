#!/usr/bin/env python

import sys
import time
import warnings
import numpy as np
import pandas as pd
import argparse as arg
from datetime import timedelta
import matplotlib.pyplot as plt
from matplotlib import ticker, gridspec, cm
from sklearn.kernel_ridge import KernelRidge
from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr, spearmanr, zscore

import util as u
from kernels import build_hybrid_kernel
from preprocess import preprocess_smiles, preprocess_fn

# data columns in the df
xcols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
        "Lambda-", "DonorFP", "AcceptorFP" ]

ycols = [ "PCE" ]


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(
                formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--train', required=True, type=str, dest='TrainDBFile',
                     help='''Training Database File.''')

    inp.add_argument('--test', required=True, type=str, dest='TestDBFile',
                     help='''Test Database File.''')

    #
    # Calculations Options
    #
    calc = parser.add_argument_group("Calculation Options")

    calc.add_argument('--alpha', required=True, type=float, dest='Alpha',
                     help='''Regularisation Hyperparameter.''')

    calc.add_argument('--gamma1', required=True, type=float, dest='Gamma1',
                     help='''Electronic Distance Hyperparameter.''')

    calc.add_argument('--gamma2', required=True, type=float, dest='Gamma2',
                     help='''Donors Tanimoto Distance Hyperparameter.''')

    calc.add_argument('--gamma3', required=True, type=float, dest='Gamma3',
                     help='''Acceptors Tanimoto Distance Hyperparameter.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")

    out.add_argument('-o', '--output', default=None, type=str, dest='OutFile',
                     help='''Output File.''')

    args = parser.parse_args()
    Opts = vars(args)

    return Opts


def plot_scatter(x, y):

    fig = plt.figure()
    gs = gridspec.GridSpec(1, 1)

    r, _ = pearsonr(x, y)
    rho, _ = spearmanr(x, y)

    ma = np.max([x.max(), y.max()]) + 1

    ax = plt.subplot(gs[0])
    ax.scatter(x, y, color="b")
    ax.tick_params(axis='both', which='major', direction='in', labelsize=22, pad=10, length=5)

    ax.set_xlabel(r"$\eta$ / %", size=24, labelpad=10)
    ax.set_ylabel(r"$\eta^{KRR}$ / %", size=24, labelpad=10)

    xtickmaj = ticker.MaxNLocator(5)
    xtickmin = ticker.AutoMinorLocator(5)
    ytickmaj = ticker.MaxNLocator(5)
    ytickmin = ticker.AutoMinorLocator(5)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)
    ax.yaxis.set_major_locator(ytickmaj)
    ax.yaxis.set_minor_locator(ytickmin)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=22, pad=10, length=2)
    ax.set_xlim(0, ma)
    ax.set_ylim(0, ma)
    ax.set_aspect('equal')

    # xmin, xmax = ax.get_xlim()
    # ymin, ymax = ax.get_ylim()
    ax.plot(np.arange(0, ma + 0.1, 0.1), np.arange(0, ma + 0.1, 0.1), color="k", ls="--")
    ax.annotate(u'$r$ = %.2f' % r, xy=(0.15,0.85), xycoords='axes fraction', size=22)
    # ax.annotate(u'$\\rho$ = %.2f' % rho, xy=(0.15,0.75), xycoords='axes fraction', size=22)

    return

def warn(*args, **kwargs):
    '''
    Suppress Scikit-learn warnings.
    '''
    pass


def main():

    warnings.warn = warn
    Opts = options()

    if Opts['OutFile'] is not None:
        sys.stdout = open(Opts['OutFile'], "wb")

    u.print_dict(Opts, title="Options")

    try:
        df_tr = pd.read_excel(Opts['TrainDBFile'], index_col=0)
    except:
        df_tr = pd.read_csv(Opts['TrainDBFile'], index_col=0)

    try:
        df_ts = pd.read_excel(Opts['TestDBFile'], index_col=0)
    except:
        df_ts = pd.read_csv(Opts['TestDBFile'], index_col=0)

    N = len(df_ts)
    df = pd.concat([df_tr, df_ts])

    # Preprocess SMILES
    df = preprocess_smiles(df)

    # Get data
    X = df[ xcols ].values
    y = df[ ycols ].values

    # Preprocess electronic data to
    # Stack fingerprints
    X = preprocess_fn(X)

    # Split in train and test set
    Xtr = X[:-N]
    ytr = y[:-N]
    Xts = X[-N:]
    yts = y[-N:]

    alpha = Opts['Alpha']
    gamma1 = Opts['Gamma1']
    gamma2 = Opts['Gamma2']
    gamma3 = Opts['Gamma3']

    # Build kernel function
    kernel = build_hybrid_kernel(gamma1=gamma1,
                                 gamma2=gamma2,
                                 gamma3=gamma3)

    # Build KRR estimator
    krr = KernelRidge(alpha=alpha, kernel=kernel)

    # Train model
    krr.fit(Xtr, ytr)

    # Predict training X and evaluate training error
    y_fit = krr.predict(Xtr)
    tr_mse = mean_squared_error(ytr.flatten(), y_fit.flatten())
    tr_rmse = np.sqrt(tr_mse)

    # Predict test X
    y_hat = krr.predict(Xts)

    try:
        mse = mean_squared_error(yts.flatten(), y_hat.flatten())
        rmse = np.sqrt(mse)
    except:
        mse = 0.0
        rmse = 0.0

    # Compute Z-scores of predictions
    y_tot = np.r_[y_fit, y_hat].flatten()
    Zscores = zscore(y_tot)[-N:]
    results = np.c_[ y_hat.flatten(), Zscores ]

    try:
        r, _ = pearsonr(yts.flatten(), y_hat.flatten())
        rho, _ = spearmanr(yts.flatten(), y_hat.flatten())
    except:
        r = 0.0
        rho = 0.0

    try:
        plot_scatter(yts.flatten(), y_hat.flatten())
        plt.savefig("data/figs/model3_infer.pdf", dpi=600, bbox_inches='tight')
    except:
        pass

    print "#"
    print "#" + u.banner(text="Model Evaluation Report", ch="-",
                         length=79)

    print "# Training RMSE      %10.6f" % tr_rmse
    print "# Test RMSE          %10.6f" % rmse
    print "# Test Pearson r     %10.6f" % r
    print "# Test Spearman rho  %10.6f" % rho
    print "#" + u.banner(ch="-", length=79)
    print "#"
    print "#" + u.banner(text="Predictions & Z-scores", ch="-",
                         length=79)
    np.savetxt(sys.stdout, results, fmt="%10.6f")

    return


if __name__ == '__main__':
    main()
