#!/usr/bin/env python

import inspect
import numpy as np


def get_hyperparams(fn):
    '''
    Function to get the arguments of a function.

    Parameters
    ----------
    fn: callable.
        function to be queried for arguments.

    Returns
    -------
    hyperparams_names: list of strings.
        list of arguments required by fn.
    '''

    hyperparams_names = inspect.getargsspec(fn)[0]

    return hyperparams_names


def gaussian_kernel(Xi, Xj, gamma):
    '''
    Function to compute a gaussian kernel.

    Parameters
    ----------
    Xi: np.array.
        training data array
    Xj: np.array.
        training/testing data array.
    gamma: float.
        hyperparameter.

    Returns
    -------
    K: np.array.
        Kernel matrix.
    '''

    m1 = Xi.shape[0]
    m2 = Xi.shape[0]
    X1 = Xi[:,np.newaxis,:]
    X1 = np.repeat(X1, m2, axis=1)
    X2 = Xj[np.newaxis,:,:]
    X2 = np.repeat(X2, m1, axis=0)
    D2 = np.sum((X1 - X2)**2, axis=2)
    K = np.exp(-gamma * D2)

    return K


def tanimoto_kernel(Xi, Xj, gamma):
    '''
    Function to compute a Tanimoto kernel.

    Parameters
    ----------
    Xi: np.array.
        training data array
    Xj: np.array.
        training/testing data array.
    gamma: float.
        hyperparameter.

    Returns
    -------
    K: np.array.
        Kernel matrix.
    '''

    m1 = Xi.shape[0]
    m2 = Xj.shape[0]
    Xii = np.repeat(np.linalg.norm(Xi, axis=1, keepdims=True)**2, m2, axis=1)
    Xjj = np.repeat(np.linalg.norm(Xj, axis=1, keepdims=True).T**2, m1, axis=0)
    T = np.dot(Xi, Xj.T) / (Xii + Xjj - np.dot(Xi, Xj.T))
    K = np.exp(-gamma * (1 - T)**2)

    return K


def build_hybrid_kernel(gamma1, gamma2, gamma3):
    '''
    Function to compute a hybrid gaussian/Tanimoto kernel within Scikit-learn.
    Wraps the real kernel function so as to accept more than 1 hyperparameter.

    Parameters
    ----------
    gamma1: float.
        gaussian kernel hyperparameter.
    gamma2: float.
        Donor Tanimoto kernel hyperparameter.
    gamma3: float.
        Acceptor Tanimoto kernel hyperparameter.

    Returns
    -------
    hybrid_kernel: callable.
        function to compute the hybrid gaussian/Tanimoto kernel given values.
    '''

    def hybrid_kernel(_x1, _x2):
        '''
        Function to compute a hybrid gaussian/Tanimoto.

        Parameters
        ----------
        _x1: np.array.
            data point.
        _x2: np.array.
            data point.

        Returns
        -------
        K: np.array.
            Kernel matrix element.
        '''

        # Split electronic data from fingerprints
        # The number of electronic properties (7) and elements of the
        # fingerprint vectors (2048) is hardcoded
        Xi_el = _x1[:7].reshape(1,-1)
        Xi_fp_d = _x1[7:2055].reshape(1,-1)
        Xi_fp_a = _x1[2055:].reshape(1,-1)

        Xj_el = _x2[:7].reshape(1,-1)
        Xj_fp_d = _x2[7:2055].reshape(1,-1)
        Xj_fp_a = _x2[2055:].reshape(1,-1)

        # Compute kernels separately
        K_el = gaussian_kernel(Xi_el, Xj_el, gamma1)
        K_fp_d = tanimoto_kernel(Xi_fp_d, Xj_fp_d, gamma2)
        K_fp_a = tanimoto_kernel(Xi_fp_a, Xj_fp_a, gamma3)

        # Element-wise multiplication
        K = K_el * K_fp_d * K_fp_a

        return K

    return hybrid_kernel


if __name__ == '__main__':

    # data columns in the df
    xcols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
            "Lambda-", "DonorFP", "AcceptorFP" ]

    ycols = [ "PCE" ]

    import pandas as pd
    df = pd.read_csv("data/db_corr.csv", index_col=0)

    # Preprocess SMILES
    from preprocess import preprocess_smiles, preprocess_fn
    df = preprocess_smiles(df)

    # Get data
    X = df[ xcols ].values
    y = df[ ycols ].values

    # Preprocess electronic data to 0 mean and 1 sigma
    # Stack fingerprints
    X = preprocess_fn(X)

    # Test vectorised implementation of Gaussian Kernel
    Xi_el = X[:,:7]
    Xj_el = X[:,:7]
    K = gaussian_kernel(Xi_el, Xj_el, 1.0)

    K_test = np.zeros((Xi_el.shape[0], Xj_el.shape[0]))
    for i in range(Xi_el.shape[0]):
        for j in range(Xj_el.shape[0]):
            xi = Xi_el[i]
            xj = Xj_el[j]
            K_test[i,j] = np.exp(-1.0 * np.linalg.norm(xi - xj)**2)

    print np.allclose(K, K_test)

    # Test vectorised implementation of Tanimoto Kernel
    Xi_fp = X[:,7:2055]
    Xj_fp = X[:,7:2055]
    K = tanimoto_kernel(Xi_fp, Xj_fp, 1.0)

    K_test = np.zeros((Xi_fp.shape[0], Xj_fp.shape[0]))
    for i in range(Xi_fp.shape[0]):
        for j in range(Xj_fp.shape[0]):
            xi = Xi_fp[i]
            xj = Xj_fp[j]
            Tij = np.dot(xi, xj) / (np.dot(xi, xi) + np.dot(xj, xj) - np.dot(xi, xj))
            K_test[i,j] = np.exp(-1.0 * (1 - Tij)**2)

    print np.allclose(K, K_test)

    # Test vectorised implementation of Tanimoto Kernel
    Xi_fp = X[:,2055:]
    Xj_fp = X[:,2055:]
    K = tanimoto_kernel(Xi_fp, Xj_fp, 1.0)

    K_test = np.zeros((Xi_fp.shape[0], Xj_fp.shape[0]))
    for i in range(Xi_fp.shape[0]):
        for j in range(Xj_fp.shape[0]):
            xi = Xi_fp[i]
            xj = Xj_fp[j]
            Tij = np.dot(xi, xj) / (np.dot(xi, xi) + np.dot(xj, xj) - np.dot(xi, xj))
            K_test[i,j] = np.exp(-1.0 * (1 - Tij)**2)

    print np.allclose(K, K_test)
