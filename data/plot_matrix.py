#!/usr/bin/env python

import sys
import warnings
import numpy as np
import pandas as pd
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem, rdMolDescriptors
from sklearn.preprocessing import StandardScaler


def warn(*args, **kwargs):
    '''
    Suppress Scikit-learn warnings.
    '''
    pass


def polish_smiles(smiles, kekule=False):
    '''
    Function to polish a SMILES string through the RDKit.

    Parameters
    ----------
    smiles: str.
        SMILES string.
    kekule: bool (default: False).
        whether to return Kekule SMILES.

    Returns
    -------
    polished: str.
        SMILES string.
    '''

    mol = Chem.MolFromSmiles(smiles)
    polished = Chem.MolToSmiles(mol, kekuleSmiles=kekule)

    return polished


def get_fp_bitvect(smiles):
    '''
    Function to convert a SMILES string to a Morgan fingerprint through the
    RDKit.

    Parameters
    ----------
    smiles: str.
        SMILES string.

    Returns
    -------
    fp_arr: np.array.
        Bit vector corresponding to the Morgan fingerpring.
    '''

    mol = Chem.MolFromSmiles(smiles)
    fp = rdMolDescriptors.GetMorganFingerprintAsBitVect(mol, 2)
    fp_arr = np.zeros((1,))
    DataStructs.ConvertToNumpyArray(fp, fp_arr)

    return fp_arr


def preprocess_smiles(df):
    '''
    Function to preprocess SMILES.

    Parameters
    ----------
    df: Pandas DataFrame.
        input data DataFrame.

    Returns
    -------
    df: Pandas DataFrame.
        preprocessed data DataFrame.
    '''

    df["DonorSMILES"] = df["DonorSMILES"].map(polish_smiles)
    df["DonorFP"] = df["DonorSMILES"].map(get_fp_bitvect)

    df["AcceptorSMILES"] = df["AcceptorSMILES"].map(polish_smiles)
    df["AcceptorFP"] = df["AcceptorSMILES"].map(get_fp_bitvect)

    return df


def preprocess_fn(X):
    '''
    Function to preprocess raw data for the KRR.

    Parameters
    ----------
    X: np.array.
        raw data array.

    Returns
    -------
    X: np.array.
        processed data array.
    '''

    X_el = X[:,:7]
    X_fp_d = np.vstack(X[:,7])
    X_fp_a = np.vstack(X[:,8])

    xscaler = StandardScaler()
    X_el = xscaler.fit_transform(X_el)

    X = np.c_[ X_el, X_fp_d, X_fp_a ]

    return X


def distance_matrix(Xi, Xj):
    '''
    Function to compute a distance matrix.

    Parameters
    ----------
    Xi: np.array.
        training data array
    Xj: np.array.
        training/testing data array.

    Returns
    -------
    D: np.array.
        distance matrix.
    '''

    m1 = Xi.shape[0]
    m2 = Xi.shape[0]
    X1 = Xi[:,np.newaxis,:]
    X1 = np.repeat(X1, m2, axis=1)
    X2 = Xj[np.newaxis,:,:]
    X2 = np.repeat(X2, m1, axis=0)
    D2 = np.sum((X1 - X2)**2, axis=2)
    D = np.sqrt(D2)

    return D


def tanimoto_distance(Xi, Xj):
    '''
    Function to compute a Tanimoto distance matrix.

    Parameters
    ----------
    Xi: np.array.
        training data array
    Xj: np.array.
        training/testing data array.

    Returns
    -------
    D: np.array.
        Tanimoto distance matrix.
    '''

    m1 = Xi.shape[0]
    m2 = Xj.shape[0]
    Xii = np.repeat(np.linalg.norm(Xi, axis=1, keepdims=True)**2, m2, axis=1)
    Xjj = np.repeat(np.linalg.norm(Xj, axis=1, keepdims=True).T**2, m1, axis=0)
    T = np.dot(Xi, Xj.T) / (Xii + Xjj - np.dot(Xi, Xj.T))
    D = 1 - T

    return D


if __name__ == '__main__':

    warnings.warn = warn

    # Set figure
    fig = plt.figure(figsize=(11.69, 11.69))

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1,3),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="7%",
                     cbar_pad=0.15)

    # data columns in the df
    cols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
           "Lambda-", "DonorFP", "AcceptorFP" ]

    # Get data
    df = pd.read_csv("db_available.csv", index_col=0)
    df = preprocess_smiles(df)

    X = df[cols].values
    y = df[["PCE"]].values

    # Preprocess electronic data to 0 mean and 1 sigma
    # Stack fingerprints
    X = preprocess_fn(X)

    # Extract data
    X_el = X[:,:7]
    X_fp_d = X[:,7:2055]
    X_fp_a = X[:,2055:]

    # Electonic distance
    D = distance_matrix(X_el, X_el)
    ax = grid[0]
    im = ax.imshow(D, cmap="gnuplot_r")

    # X axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_xticklabels([])
    ax.set_xticks([])
    ax.set_xlabel("Electronic\n Distance", size=24, labelpad=15)
    ax.xaxis.set_label_position('top')

    # Y axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_yticklabels([])
    ax.set_yticks([])
    ax.set_aspect('equal')

    # Donor Fingerprint similarity
    D = tanimoto_distance(X_fp_d, X_fp_d)
    ax = grid[1]
    im = ax.imshow(D, cmap="gnuplot_r")

    # X axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_xticklabels([])
    ax.set_xticks([])
    ax.set_xlabel("Donors\n Distance", size=24, labelpad=15)
    ax.xaxis.set_label_position('top')

    # Y axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_yticklabels([])
    ax.set_yticks([])
    ax.set_aspect('equal')

    # Acceptor Fingerprint similarity
    D = tanimoto_distance(X_fp_a, X_fp_a)
    ax = grid[2]
    im = ax.imshow(D, cmap="gnuplot_r")

    # X axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_xticklabels([])
    ax.set_xticks([])
    ax.set_xlabel("Acceptors\n Distance", size=24, labelpad=15)
    ax.xaxis.set_label_position('top')

    # Y axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_yticklabels([])
    ax.set_yticks([])
    ax.set_aspect('equal')

    # Colorbar
    sm = plt.cm.ScalarMappable(cmap="gnuplot_r")
    sm._A = []
    ax.cax.colorbar(im)
    ax.cax.set_ylabel("Distance", size=24, labelpad=10)
    ax.cax.set_yticklabels([0.0, 0.2, 0.4, 0.6, 0.8, 1.0], size=22)
    ax.cax.toggle_label(True)

    plt.savefig("figs/distances.pdf", dpi=600, bbox_inches='tight')
    # plt.show()
