#!/usr/bin/env python

import numpy as np
from scipy.constants import *

# Conversion factor and solar spectrum.
eV2nm = 1239.8
spectrum = np.loadtxt("ASTMG173.csv")
x_spec = spectrum[:,0]
y_spec = spectrum[:,1]


def scharber(homod, lumod, lumoa, loss=0.3, fac=0.65, FF=0.65, Pin=1000.0):
    '''
    Function to compute the efficiency of a donor-acceptor pair according to
    Scharber's model.

    Parameters
    ----------
    homod: float.
        donor's HOMO energy.
    lumod: float.
        donor's LUMO energy.
    lumoa: float.
        acceptor's LUMO energy.
    loss: float.
        empirical loss factor for charge separation.
    fac: float.
        empirical scaling factor for light absorption.
    FF: float.
        empirical fill factor.
    Pin: float.
        incident light power.

    Returns
    -------
    eta: float.
        % efficiency of the solar cell with the given electronic parameters
        estimated according to Scharber.
    '''

    DA_gap = lumoa - homod
    D_gap = lumod - homod

    Voc_Sch = scharber_Voc(DA_gap, loss)
    Jsc_Sch = scharber_Jsc(D_gap, fac)
    eta = Voc_Sch * Jsc_Sch * FF / Pin

    return eta * 100.0


def scharber_Voc(gap, loss=0.3):
    '''
    Function to compute the open circuit voltage of a donor-acceptor pair
    according to Scharber's model.

    Parameters
    ----------
    gap: float.
        energy gap between the acceptor's LUMO and donor's HOMO.
    loss: float.
        empirical loss factor for the model.

    Returns
    -------
    Voc: float.
        open circuit voltage.
    '''

    Voc = gap - loss

    return Voc


def scharber_Jsc(gap, fac=0.65):
    '''
    Function to compute the short circuit current of a donor-acceptor pair
    according to Scharber's model.

    Parameters
    ----------
    gap: float.
        energy gap between the donor's LUMO and donor's HOMO.

    Returns
    -------
    Jsc: float.
        short circuit current.
    '''

    wavelength = eV2nm / gap

    idxs = (x_spec <= wavelength)
                                                 
    # Convert to SI units (nm -> m).
    newx = np.copy(x_spec)[idxs] * 1e-9
    newy = np.copy(y_spec)[idxs] / 1e-9

    # This uses some constants from SciPy, e, h, c.
    Jph = e * np.trapz(newy * newx / (h * c), x=newx)
    
    # Convert from A / m**2 to mA / cm**2.
    # Jph /= 10.0

    # Scale empirically.
    Jsc = Jph * fac

    return Jsc


if __name__ == '__main__':

    import csv
    import pandas as pd
    # df = pd.read_csv("../data/db_corr.csv", index_col=0)
    # cols = [ "HOMO_D", "LUMO_D", "LUMO_A", "V_OC","J_SC","PCE" ]
    df = pd.read_csv("lit_tests.csv", index_col=0)
    cols = [ "HOMO_D", "LUMO_D", "LUMO_A" ]
    df1 = df[cols]
    df1["DA_GAP"] = df["LUMO_A"] - df["HOMO_D"]
    df1["D_GAP"] = df["LUMO_D"] - df["HOMO_D"]
    df["Scharber_V_OC"] = df1["DA_GAP"].map(scharber_Voc)
    df["Scharber_J_SC"] = df1["D_GAP"].map(scharber_Jsc)
    df["Scharber_PCE"] = 100 * df["Scharber_V_OC"] * df["Scharber_J_SC"] * 0.65 / 1000
    df["Scharber_J_SC"] /= 10

    df.to_csv("lit_tests.csv", quoting=csv.QUOTE_NONNUMERIC)
