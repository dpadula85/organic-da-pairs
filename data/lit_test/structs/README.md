Description
===========
In this folder there are:

- the optimised geometries of donors from literature tests, obtained with the
Gaussian16 software at B3LYP/6-31G\* level (`donors.xyz`)

- the optimised geometries of acceptors from literature tests, obtained with
the Gaussian16 software at B3LYP/6-31G\* level (`acceptors.xyz`)

- their SMILES representations for easy visualisation.
