Description
===========
In this folder there are:

- the database for experimentally available donor-acceptor pairs
used to train models, with results of cross-validation (`db_available.csv`)

- the database for experimentally unavailable donor-acceptor pairs, with
results of our predictions (`db_unexplored.csv`)

- scripts to produce plots

- results of the training

Notice that figures will be saved in `.pdf` format in the
`figs/` folder.
