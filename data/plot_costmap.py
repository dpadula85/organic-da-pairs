#!/usr/bin/env python

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import AxesGrid, ImageGrid
from matplotlib import ticker, gridspec, colors

from numpy import ma
from matplotlib import cbook
from matplotlib.colors import Normalize


class MidPointNorm(Normalize):

    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
        Normalize.__init__(self,vmin, vmax, clip)
        self.midpoint = midpoint

    def __call__(self, value, clip=None):
        if clip is None:
            clip = self.clip

        result, is_scalar = self.process_value(value)

        self.autoscale_None(result)
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if not (vmin < midpoint < vmax):
            raise ValueError("midpoint must be between maxvalue and minvalue.")
        elif vmin == vmax:
            result.fill(0) # Or should it be all masked? Or 0.5?
        elif vmin > vmax:
            raise ValueError("maxvalue must be bigger than minvalue")
        else:
            vmin = float(vmin)
            vmax = float(vmax)
            if clip:
                mask = ma.getmask(result)
                result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                  mask=mask)

            # ma division is very slow; we can take a shortcut
            resdat = result.data

            #First scale to -1 to 1 range, than to from 0 to 1.
            resdat -= midpoint
            resdat[resdat>0] /= abs(vmax - midpoint)
            resdat[resdat<0] /= abs(vmin - midpoint)

            resdat /= 2.
            resdat += 0.5
            result = ma.array(resdat, mask=result.mask, copy=False)

        if is_scalar:
            result = result[0]
        return result

    def inverse(self, value):
        if not self.scaled():
            raise ValueError("Not invertible until scaled")
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if cbook.iterable(value):
            val = ma.asarray(value)
            val = 2 * (val-0.5)
            val[val>0]  *= abs(vmax - midpoint)
            val[val<0] *= abs(vmin - midpoint)
            val += midpoint
            return val
        else:
            val = 2 * (val - 0.5)
            if val < 0:
                return  val*abs(vmin-midpoint) + midpoint
            else:
                return  val*abs(vmax-midpoint) + midpoint


if __name__ == '__main__':

    data = np.loadtxt("model1_costmap.dat")
    x = data[:,1:-1]
    y = data[:,0]
    z = data[:,-1]

    # Scatter plot
    fig = plt.figure(figsize=(11.69, 11.69))

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1,2),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="7%",
                     cbar_pad=0.15)

    gs = gridspec.GridSpec(1, 2, width_ratios=[20,1])
    ax = plt.subplot(gs[0], projection="3d")

    sc = ax.scatter(x[:,0], x[:,1], x[:,2], s=(2 - y) * 70, c=z, cmap="gnuplot_r", alpha=0.4)

    sizes = np.arange(0, 2, 0.5)
    labels = [ u"$\\alpha = %.1f$" % s for s in sizes ]
    points = [ ax.scatter([], [], [], s=(2 - s) * 70, c='gray') for s in sizes ]


    # Fancy stuff
    ax.set_xlabel(u"\n\n$\gamma_1$", size=24)
    ax.set_ylabel(u"\n\n$\gamma_2$", size=24)
    ax.set_zlabel(u"\n\n$\gamma_3$", size=24)

    ax.tick_params(axis='both', which='major', direction='in', labelsize=24, pad=10, length=7)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=24, pad=10, length=3)

    xtickmaj = ticker.MaxNLocator(5)
    xtickmin = ticker.AutoMinorLocator(5)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)

    ytickmaj = ticker.MaxNLocator(5)
    ytickmin = ticker.AutoMinorLocator(5)
    ax.yaxis.set_major_locator(ytickmaj)
    ax.yaxis.set_minor_locator(ytickmin)

    ztickmaj = ticker.MaxNLocator(5)
    ztickmin = ticker.AutoMinorLocator(5)
    ax.zaxis.set_major_locator(ztickmaj)
    ax.zaxis.set_minor_locator(ztickmin)
    ax.legend(points, labels, scatterpoints=1, fontsize=24, loc=9,
              bbox_to_anchor=(0.5,1.10), ncol=2).draw_frame(False)

    # Show colorbar
    cbax = plt.subplot(gs[1])
    cb = plt.colorbar(sc, cax=cbax)
    cb.set_label(u"RMSE", size=24, rotation="vertical", labelpad=10)
    cb.ax.tick_params(labelsize=24, pad=10, direction='in')

    # plt.tight_layout()
    outname = "model1_costmap.pdf"
    plt.savefig("figs/%s" % outname, dpi=600, bbox_inches='tight')
    # plt.show()

    data = np.loadtxt("model2_costmap.dat")
    x = data[:,1:-1]
    y = data[:,0]
    z = data[:,-1]

    # Scatter plot
    fig = plt.figure(figsize=(11.69, 11.69))

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1,2),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="7%",
                     cbar_pad=0.15)

    gs = gridspec.GridSpec(1, 2, width_ratios=[20,1])
    ax = plt.subplot(gs[0], projection="3d")

    sc = ax.scatter(x[:,0], x[:,1], x[:,2], s=(2 - y) * 70, c=z, cmap="gnuplot_r", alpha=0.4)

    sizes = np.arange(0, 2, 0.5)
    labels = [ u"$\\alpha = %.1f$" % s for s in sizes ]
    points = [ ax.scatter([], [], [], s=(2 - s) * 70, c='gray') for s in sizes ]


    # Fancy stuff
    ax.set_xlabel(u"\n\n$\gamma_1$", size=24)
    ax.set_ylabel(u"\n\n$\gamma_2$", size=24)
    ax.set_zlabel(u"\n\n$\gamma_3$", size=24)

    ax.tick_params(axis='both', which='major', direction='in', labelsize=24, pad=10, length=7)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=24, pad=10, length=3)

    xtickmaj = ticker.MaxNLocator(5)
    xtickmin = ticker.AutoMinorLocator(5)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)

    ytickmaj = ticker.MaxNLocator(5)
    ytickmin = ticker.AutoMinorLocator(5)
    ax.yaxis.set_major_locator(ytickmaj)
    ax.yaxis.set_minor_locator(ytickmin)

    ztickmaj = ticker.MaxNLocator(5)
    ztickmin = ticker.AutoMinorLocator(5)
    ax.zaxis.set_major_locator(ztickmaj)
    ax.zaxis.set_minor_locator(ztickmin)
    ax.legend(points, labels, scatterpoints=1, fontsize=24, loc=9,
              bbox_to_anchor=(0.5,1.10), ncol=2).draw_frame(False)

    # Show colorbar
    cbax = plt.subplot(gs[1])
    cb = plt.colorbar(sc, cax=cbax)
    cb.set_label(u"$w$RMSE", size=24, rotation="vertical", labelpad=10)
    cb.ax.tick_params(labelsize=24, pad=10, direction='in')

    # plt.tight_layout()
    outname = "model2_costmap.pdf"
    plt.savefig("figs/%s" % outname, dpi=600, bbox_inches='tight')
    # plt.show()

    data = np.loadtxt("model3_costmap.dat")
    x = data[:,1:-1]
    y = data[:,0]
    z = data[:,-1]

    # Scatter plot
    fig = plt.figure(figsize=(11.69, 11.69))

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1,2),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="7%",
                     cbar_pad=0.15)

    gs = gridspec.GridSpec(1, 2, width_ratios=[20,1])
    ax = plt.subplot(gs[0], projection="3d")

    sc = ax.scatter(x[:,0], x[:,1], x[:,2], s=(2 - y) * 70, c=z, cmap="gnuplot_r", alpha=0.4)

    sizes = np.arange(0, 2, 0.5)
    labels = [ u"$\\alpha = %.1f$" % s for s in sizes ]
    points = [ ax.scatter([], [], [], s=(2 - s) * 70, c='gray') for s in sizes ]


    # Fancy stuff
    ax.set_xlabel(u"\n\n$\gamma_1$", size=24)
    ax.set_ylabel(u"\n\n$\gamma_2$", size=24)
    ax.set_zlabel(u"\n\n$\gamma_3$", size=24)

    ax.tick_params(axis='both', which='major', direction='in', labelsize=24, pad=10, length=7)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=24, pad=10, length=3)

    xtickmaj = ticker.MaxNLocator(5)
    xtickmin = ticker.AutoMinorLocator(5)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)

    ytickmaj = ticker.MaxNLocator(5)
    ytickmin = ticker.AutoMinorLocator(5)
    ax.yaxis.set_major_locator(ytickmaj)
    ax.yaxis.set_minor_locator(ytickmin)

    ztickmaj = ticker.MaxNLocator(5)
    ztickmin = ticker.AutoMinorLocator(5)
    ax.zaxis.set_major_locator(ztickmaj)
    ax.zaxis.set_minor_locator(ztickmin)
    ax.legend(points, labels, scatterpoints=1, fontsize=24, loc=9,
              bbox_to_anchor=(0.5,1.10), ncol=2).draw_frame(False)

    # Show colorbar
    cbax = plt.subplot(gs[1])
    cb = plt.colorbar(sc, cax=cbax)
    cb.set_label(u"$w$RMSE", size=24, rotation="vertical", labelpad=10)
    cb.ax.tick_params(labelsize=24, pad=10, direction='in')

    # plt.tight_layout()
    outname = "model3_costmap.pdf"
    plt.savefig("figs/%s" % outname, dpi=600, bbox_inches='tight')
    # plt.show()
