#!/usr/bin/env python

import warnings
warnings.filterwarnings("ignore")

import sys
import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt
from matplotlib import ticker


def plot_dist(x):

    nbins = int(np.log2(x.shape[0]) + 1)
    avg = np.mean(x)
    sigma = np.std(x)

    fig, ax = plt.subplots(figsize=(19.2,11))
    n, bins, patches = ax.hist(x, bins=nbins, histtype='bar', rwidth=0.75,
                               hatch='//', fill=False, color='b', edgecolor='b')


    # Fancy stuff
    ax.set_xlabel(u"$\eta$ / %", size=24)
    ax.set_ylabel(u"Count", size=24)
    ax.tick_params(axis='both', which='major', direction='in', labelsize=24, pad=10, length=7)
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=24, pad=10, length=3)
    xtickmaj = ticker.MultipleLocator(2)
    xtickmin = ticker.MultipleLocator(0.5)
    ytickmaj = ticker.MultipleLocator(1000)
    ytickmin = ticker.MultipleLocator(200)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.minorticks_on()
    ax.annotate(u'$\eta_{avg}$ = %.2f' % avg, xy=(0.55,0.8), xycoords='axes fraction', size=24)
    ax.annotate(u'$\sigma_{\eta}$ = %.2f' % sigma, xy=(0.55,0.7), xycoords='axes fraction', size=24)

    return fig, ax


if __name__ == '__main__':

    df = pd.read_csv("db_unexplored.csv", index_col=0)
    df = df[df["Model1_PCE"] > 0 ]
    x = df["Model1_PCE"]
    plot_dist(x)
    plt.savefig("figs/model1_unexplored_distribution.pdf", dpi=600, bbox_inches='tight')

    x = df["Model2_PCE"]
    plot_dist(x)
    plt.savefig("figs/model2_unexplored_distribution.pdf", dpi=600, bbox_inches='tight')

    x = df["Model3_PCE"]
    plot_dist(x)
    plt.savefig("figs/model3_unexplored_distribution.pdf", dpi=600, bbox_inches='tight')
