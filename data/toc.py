#!/usr/bin/env python

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm, gridspec
from mpl_toolkits.axes_grid1 import ImageGrid
from matplotlib.ticker import FormatStrFormatter


def plot_space(M):

    fig = plt.figure(figsize=(11.69, 11.69))

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1,1),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="2%",
                     cbar_pad=0.15)

    ax = grid[0]
    im = ax.imshow(M, cmap="gnuplot", vmin=0, vmax=m)

    # X axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_xticklabels([])
    ax.set_xticks([])
    ax.set_xlabel("Donors", size=24, labelpad=15)
    ax.xaxis.set_label_position('top')

    # Y axis
    # Hide main labels and assign to minor labels their value
    # This way label will appear in the middle of the matrix element
    ax.set_yticklabels([])
    ax.set_yticks([])
    ax.set_ylabel("Acceptors", size=24, labelpad=15)

    # # Colorbar
    sm = plt.cm.ScalarMappable(cmap="gnuplot")
    sm._A = []
    ax.cax.colorbar(im)
    ax.cax.set_ylabel("Photovoltaic Efficiency", size=24, labelpad=10)
    ax.cax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax.cax.tick_params(labelsize=22)
    ax.cax.toggle_label(True)

    return fig

if __name__ == '__main__':

    # Get data
    df = pd.read_csv("db_available.csv", index_col=0)
    combs = df[["DonorNo", "AcceptorNo", "PCE"]].values
    shape = combs[:,:2].max(axis=0).astype(int)
    M = np.zeros(shape)
    for comb in combs:
        didx, aidx = comb[:2].astype(int) - 1
        M[didx,aidx] = comb[-1]

    # Transpose for prettier plot
    M = M.T
    m = np.ceil(M.max())
    M[M == 0] = None

    plot_space(M)
    plt.savefig("figs/toc_expt.pdf", dpi=600, bbox_inches='tight')

    # Get data
    df = pd.read_csv("db_available.csv", index_col=0)
    df1 = pd.read_csv("db_unexplored.csv", index_col=0)
    df = pd.concat([df, df1])

    combs = df[["DonorNo", "AcceptorNo", "Model3_PCE"]].values
    shape = combs[:,:2].max(axis=0).astype(int)
    M = np.zeros(shape)
    for comb in combs:
        didx, aidx = comb[:2].astype(int) - 1
        M[didx,aidx] = comb[-1]

    # Transpose for prettier plot
    M = M.T
    m = np.ceil(M.max())
    M[M < 0] = 0.0

    plot_space(M)
    plt.savefig("figs/toc_pred.pdf", dpi=600, bbox_inches='tight')
