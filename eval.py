#!/usr/bin/env python

import sys
import time
import warnings
import numpy as np
import pandas as pd
import argparse as arg
from datetime import timedelta
import matplotlib.pyplot as plt
from matplotlib import ticker, gridspec, cm
from scipy.stats import pearsonr, spearmanr
from sklearn.kernel_ridge import KernelRidge
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import LeaveOneOut

import util as u
from kernels import build_hybrid_kernel
from preprocess import preprocess_smiles, preprocess_fn

# data columns in the df
xcols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
        "Lambda-", "DonorFP", "AcceptorFP" ]

ycols = [ "PCE" ]


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(
                formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--db', required=True, type=str, dest='DBFile',
                     help='''Database File.''')

    #
    # Calculations Options
    #
    calc = parser.add_argument_group("Calculation Options")

    calc.add_argument('--alpha', required=True, type=float, dest='Alpha',
                     help='''Regularisation Hyperparameter.''')

    calc.add_argument('--gamma1', required=True, type=float, dest='Gamma1',
                     help='''Electronic Distance Hyperparameter.''')

    calc.add_argument('--gamma2', required=True, type=float, dest='Gamma2',
                     help='''Donors Tanimoto Distance Hyperparameter.''')

    calc.add_argument('--gamma3', required=True, type=float, dest='Gamma3',
                     help='''Acceptors Tanimoto Distance Hyperparameter.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")

    out.add_argument('-o', '--output', default=None, type=str, dest='OutFile',
                     help='''Output File.''')

    args = parser.parse_args()
    Opts = vars(args)

    return Opts


def plot_scatter(x, y):

    fig = plt.figure()
    gs = gridspec.GridSpec(1, 1)

    r, _ = pearsonr(x, y)
    rho, _ = spearmanr(x, y)

    ma = np.max([x.max(), y.max()]) + 1

    ax = plt.subplot(gs[0])
    ax.scatter(x, y, color="b")
    ax.tick_params(axis='both', which='major', direction='in', labelsize=22, pad=10, length=5)

    ax.set_xlabel(r"$\eta$ / %", size=24, labelpad=10)
    ax.set_ylabel(r"$\eta^{KRR}$ / %", size=24, labelpad=10)

    xtickmaj = ticker.MaxNLocator(5)
    xtickmin = ticker.AutoMinorLocator(5)
    ytickmaj = ticker.MaxNLocator(5)
    ytickmin = ticker.AutoMinorLocator(5)
    ax.xaxis.set_major_locator(xtickmaj)
    ax.xaxis.set_minor_locator(xtickmin)
    ax.yaxis.set_major_locator(ytickmaj)
    ax.yaxis.set_minor_locator(ytickmin)
    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.tick_params(axis='both', which='minor', direction='in', labelsize=22, pad=10, length=2)
    ax.set_xlim(0, ma)
    ax.set_ylim(0, ma)
    ax.set_aspect('equal')

    # xmin, xmax = ax.get_xlim()
    # ymin, ymax = ax.get_ylim()
    ax.plot(np.arange(0, ma + 0.1, 0.1), np.arange(0, ma + 0.1, 0.1), color="k", ls="--")
    ax.annotate(u'$r$ = %.2f' % r, xy=(0.15,0.85), xycoords='axes fraction', size=22)
    # ax.annotate(u'$\\rho$ = %.2f' % rho, xy=(0.15,0.75), xycoords='axes fraction', size=22)

    return

def warn(*args, **kwargs):
    '''
    Suppress Scikit-learn warnings.
    '''
    pass


def main():

    warnings.warn = warn
    Opts = options()

    if Opts['OutFile'] is not None:
        sys.stdout = open(Opts['OutFile'], "wb")

    u.print_dict(Opts, title="Options")

    try:
        df = pd.read_excel(Opts['DBFile'], index_col=0)
    except:
        df = pd.read_csv(Opts['DBFile'], index_col=0)

    # Preprocess SMILES
    df = preprocess_smiles(df)

    # Get data
    X = df[ xcols ].values
    y = df[ ycols ].values

    # Preprocess electronic data to
    # Stack fingerprints
    X = preprocess_fn(X)

    alpha = Opts['Alpha']
    gamma1 = Opts['Gamma1']
    gamma2 = Opts['Gamma2']
    gamma3 = Opts['Gamma3']

    # Build kernel function
    kernel = build_hybrid_kernel(gamma1=gamma1,
                                 gamma2=gamma2,
                                 gamma3=gamma3)

    # Build KRR estimator
    krr = KernelRidge(alpha=alpha, kernel=kernel)

    # LOO loop
    loo = LeaveOneOut()
    y_true = []
    y_pred = []
    tr_errs = []
    for i, idxs in enumerate(loo.split(X), start=1):

        # Split data
        tr_idx, ts_idx = idxs
        Xtr = X[tr_idx]
        ytr = y[tr_idx]
        Xts = X[ts_idx]
        yts = y[ts_idx]

        # Train model
        krr.fit(Xtr, ytr)

        # Predict training X and evaluate training error
        y_fit = krr.predict(Xtr)
        tr_mse = mean_squared_error(ytr, y_fit)
        tr_rmse = np.sqrt(tr_mse)
        tr_errs.append(tr_rmse)

        # Predict test X
        y_hat = krr.predict(Xts)
        y_true.append(yts)
        y_pred.append(y_hat)

    tr_errs = np.array(tr_errs).flatten()
    y_true = np.array(y_true).flatten()
    y_pred = np.array(y_pred).flatten()

    mse = mean_squared_error(y_true, y_pred)
    rmse = np.sqrt(mse)

    r, _ = pearsonr(y_true, y_pred)
    rho, _ = spearmanr(y_true, y_pred)

    plot_scatter(y_true, y_pred)
    plt.savefig("data/figs/eval_model3.pdf", dpi=600, bbox_inches='tight')

    print "#"
    print "#" + u.banner(text="Model Evaluation Report", ch="-",
                         length=79)

    print "# Avg. Training RMSE             %10.6f" % np.mean(tr_errs)
    print "# Cross Validation RMSE          %10.6f" % rmse
    print "# Cross Validation Pearson r     %10.6f" % r
    print "# Cross Validation Spearman rho  %10.6f" % rho
    print "#" + u.banner(ch="-", length=79)
    print "#"
    print "#" + u.banner(text="Predictions", ch="-",
                         length=79)
    np.savetxt(sys.stdout, y_pred, fmt="%10.6f")

    return


if __name__ == '__main__':
    main()
