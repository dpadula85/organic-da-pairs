#!/usr/bin/env python

import numpy as np
import pandas as pd
from rdkit import Chem, DataStructs
from rdkit.Chem import rdMolDescriptors
from sklearn.preprocessing import StandardScaler


def polish_smiles(smiles, kekule=False):
    '''
    Function to polish a SMILES string through the RDKit.

    Parameters
    ----------
    smiles: str.
        SMILES string.
    kekule: bool (default: False).
        whether to return Kekule SMILES.

    Returns
    -------
    polished: str.
        SMILES string.
    '''

    mol = Chem.MolFromSmiles(smiles)
    polished = Chem.MolToSmiles(mol, kekuleSmiles=kekule)

    return polished


def get_fp_bitvect(smiles):
    '''
    Function to convert a SMILES string to a Morgan fingerprint through the
    RDKit.

    Parameters
    ----------
    smiles: str.
        SMILES string.

    Returns
    -------
    fp_arr: np.array.
        Bit vector corresponding to the Morgan fingerpring.
    '''

    mol = Chem.MolFromSmiles(smiles)
    fp = rdMolDescriptors.GetMorganFingerprintAsBitVect(mol, 2)
    fp_arr = np.zeros((1,))
    DataStructs.ConvertToNumpyArray(fp, fp_arr)

    return fp_arr


def preprocess_smiles(df):
    '''
    Function to preprocess SMILES.

    Parameters
    ----------
    df: Pandas DataFrame.
        input data DataFrame.

    Returns
    -------
    df: Pandas DataFrame.
        preprocessed data DataFrame.
    '''

    df["DonorSMILES"] = df["DonorSMILES"].map(polish_smiles)
    df["DonorFP"] = df["DonorSMILES"].map(get_fp_bitvect)

    df["AcceptorSMILES"] = df["AcceptorSMILES"].map(polish_smiles)
    df["AcceptorFP"] = df["AcceptorSMILES"].map(get_fp_bitvect)

    return df


def preprocess_fn(X):
    '''
    Function to preprocess raw data for the KRR.

    Parameters
    ----------
    X: np.array.
        raw data array.

    Returns
    -------
    X: np.array.
        processed data array.
    '''

    X_el = X[:,:7]
    X_fp_d = np.vstack(X[:,7])
    X_fp_a = np.vstack(X[:,8])

    xscaler = StandardScaler()
    X_el = xscaler.fit_transform(X_el)

    X = np.c_[ X_el, X_fp_d, X_fp_a ]

    return X


if __name__ == '__main__':

    # data columns in the df
    cols = [ "HOMO_D", "LUMO_D", "Lambda+", "HOMO_A", "LUMO_A", "LUMO+1_A",
           "Lambda-", "DonorFP", "AcceptorFP" ]

    df = pd.read_csv("db_corr.csv", index_col=0)
    df = preprocess_smiles(df)

    X = df[cols].values
    y = df[["PCE"]].values

    # Preprocess electronic data to 0 mean and 1 sigma
    # Stack fingerprints
    X = preprocess_fn(X)
