#!/usr/bin/env python

import argparse as arg


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(
                formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--db', required=True, type=str, dest='DBFile',
                     help='''Database File.''')

    #
    # Calculations Options
    #
    calc = parser.add_argument_group("Calculation Options")

    calc.add_argument('--weighted', default=False, action="store_true",
                      dest='Weighted', help='''Database File.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")

    out.add_argument('-o', '--output', default=None, type=str, dest='OutFile',
                     help='''Output File.''')

    out.add_argument('-v', '--verbosity',
                     default=0, action="count", dest="Verb",
                     help='''Verbosity level''')

    args = parser.parse_args()
    Opts = vars(args)

    return Opts


if __name__ == '__main__':
    pass
